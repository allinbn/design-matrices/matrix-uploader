import os.path
import json
from envparse import Env
from slugify import slugify
import shutil
from pprint import pprint


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def exit_with(*message, exit_code=1):
    print(' '.join(message))
    exit(exit_code)

def main():
    """
    Takes a parameter and converts.
    :return:
    """

    trigger_payload_path = env('TRIGGER_PAYLOAD', '')
    payload = None
    if trigger_payload_path:
        if os.path.isfile(trigger_payload_path):
            with open(env('TRIGGER_PAYLOAD')) as f:
                data = f.read()
        else:
            exit_with('Not a file:', trigger_payload_path)

        try:
            payload = json.loads(data)
        except json.decoder.JSONDecodeError:
            pprint(data)

        if payload['event_type'] != 'issue':
            exit_with('not issue')

        pprint(payload)

    elif env('ISSUE_NAME'):
        file_name = slugify(env("ISSUE_NAME"), lowercase=False)
        print(file_name)
        shutil.copyfile('matrix.png', file_name + '.png')


if __name__ == '__main__':
    main()
